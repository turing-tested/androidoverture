package com.example.mdjournal.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mdjournal.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Document;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import kotlin.Metadata;

/// las variables con v son las vistas
//las variables con txt, los datos donde se guardan los valores 


public class Login extends AppCompatActivity {
    //Intro
    Button blogin;
    Button bRegister;
    EditText vtxtUser;
    EditText vTxtPass;
    String TAG = "Info";
    FirebaseFirestore db;
    String txtUser;
    String txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        db.clearPersistence();
        bRegister.setOnClickListener(this::onClick);
        blogin.setOnClickListener(this::onClick);
    }


    public void init() {
        blogin = findViewById(R.id.bLogin);
        bRegister = findViewById(R.id.bRegister);
        vtxtUser = findViewById(R.id.txtUsuario);
        vTxtPass = findViewById(R.id.txtContra);
        db = FirebaseFirestore.getInstance();
        txtUser = "";
        txtPass = "";

    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View v) {    //metodo para poner todos los lisenter de la de golpe

        switch (v.getId()/*to get clicked view id**/) {
            case R.id.bLogin:
                txtUser = vtxtUser.getText().toString();
                txtPass = vTxtPass.getText().toString();
                if (txtPass.isEmpty() || txtUser.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("los datos son incorrectos")
                            .setTitle("Error");
                    builder.show();

                } else {

                    login(txtUser, txtPass);


                }


                break;
            case R.id.bRegister:
                txtUser = vtxtUser.getText().toString();
                txtPass = vTxtPass.getText().toString();
                if ((txtUser.isEmpty() || (txtPass.isEmpty()))) {
                    Toast.makeText(this, "Has de rellenar todos los campos", Toast.LENGTH_LONG + 2).show();
                    vtxtUser.setText("");
                    vTxtPass.setText("");
                }else{
                    register(txtUser,txtPass);
                }

                break;

            default:
                break;
        }
    }

    // Read from the database

    public int register(String user, String pass) {
        Map<String, Object> muser = new HashMap<>();
        muser.put("user", user);
        muser.put("password", pass);
        db.collection("Logins").document(user).set(muser);
        vTxtPass.setText("");
        vtxtUser.setText("");

        return 1;
    }

    public void login(String user, String password) {
        DocumentReference rf = db.collection("Logins").document(user);
        rf.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot docu = task.getResult();
                if(docu.exists()){
                    String xpassword = (String) docu.getData().get("password");

                    if(password.equals(xpassword)){
                        Toast.makeText(Login.this, "adentro", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("usuario",user);

                        startActivity(intent);


                        finish();
                    }else{
                        Toast.makeText(Login.this, "contraseña incorrecta", Toast.LENGTH_SHORT).show();

                    }

                }else{
                    Toast.makeText(Login.this, "el usuario no existe", Toast.LENGTH_SHORT).show();
                }
                vTxtPass.setText("");
                vtxtUser.setText("");
                txtUser = "";
                txtPass = "";
            }
        });


    }



}
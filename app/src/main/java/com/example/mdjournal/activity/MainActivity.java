package com.example.mdjournal.activity;

import androidx.appcompat.app.AppCompatActivity;
import com.example.mdjournal.*;

import android.content.pm.LauncherApps;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {
    String user;
    TextView vtTxtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }


    public void init(){
        vtTxtName = findViewById(R.id.txtUserName);

        Bundle extras = getIntent().getExtras();
        user = extras.getString("usuario");
        Toast.makeText(this, user, Toast.LENGTH_SHORT).show();
        vtTxtName.setText(user);
    }

}